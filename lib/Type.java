package lib;

/**
 * Created by Administrator on 2016/4/2.
 */
public class Type {
	public Symbol name;
	public int dms;
	public Type() {}
	public boolean equals(Type t) {
		return name == t.name && dms == t.dms;
	}
	public Type(String n, int d) {
		name = Symbol.symbol(n);
		dms = d;
	}
	public Type(String n) {
		name = Symbol.symbol(n);
		dms = 0;
	}
	public Type(Symbol n, int d) {
		name = n;
		dms = d;
	}
	public String toString() {
		return name.toString() + " " + String.valueOf(dms);
	}
}
