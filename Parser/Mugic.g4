grammar Mugic;

@header {
package Parser;
}

stmt
	:	compoundStmt				# compound
	|	forStmt						# for
	|	whileStmt					# while
	|	ifStmt						# if
	|	jumpStmt					# jump
	|	variableDefinition2			# varDef
	|	expr ';'					# ep
	|	';'							# nullExp
	;

compoundStmt
	:	'{' (stmt)* '}'
	;

forStmt
	:	'for' '(' (exp1 = expr?) ';' (exp2 = expr?) ';' (exp3 = expr?) ')' stmt
	;

whileStmt
	:	'while' '(' expr ')' stmt
	;

ifStmt
	:	'if' '(' expr ')' stmt1 = stmt ('else' stmt2 = stmt)?
	;

jumpStmt
	:	'break' ';'			# Break
	|	'continue' ';'		# Continue
	|	'return' expr? ';'	# Return
	;

expr
	:	assignmentExpr
	;

primaryExpr
	:	ID											# VariableExpr
	|	ID '()'										# FuncExpr1
	|	ID '(' expr (',' expr)* ')'					# FuncExpr2
	|	constant									# ConstantExpr
	|	'(' expr ')'								# ParenExpr
	|	New type = (TypeName | ID) (('[' expr ']') | Braket) *	# CreationExpr
	;

New : 'new';

postfixExpr
    :   primaryExpr									# prExpr
    |   postfixExpr '[' expr ']'					# arrayExpr
    |   postfixExpr Dot primaryExpr					# classExpr
    |   postfixExpr op = ('++' | '--')				# rightExpr
    ;

unaryExpr
	:	postfixExpr
	|	op = ('!' | '~' | '+' | '-' | '++' | '--') unaryExpr
	;

multiplicativeExpr
	:	unaryExpr
	|	multiplicativeExpr op = ('*' | '/' | '%') unaryExpr
	;

additiveExpr
	:	multiplicativeExpr
	|	additiveExpr op = ('+' | '-') multiplicativeExpr
	;

shiftExpr
	:	additiveExpr
	|	shiftExpr op = ('<<' | '>>') additiveExpr
	;

relationalExpr
	:	shiftExpr
	|	relationalExpr op = ('<' | '>' | '<=' | '>=') shiftExpr
	;

equalityExpr
	:	relationalExpr
	|	equalityExpr op = ('==' | '!=') relationalExpr
	;

andExpr
	:	equalityExpr
	|	andExpr op = '&' equalityExpr
	;

exclusiveOrExpr
	:	andExpr
	|	exclusiveOrExpr op = '^' andExpr
	;

inclusiveOrExpr
	:	exclusiveOrExpr
	|	inclusiveOrExpr op = '|' exclusiveOrExpr
	;

logicalAndExpr
	:	inclusiveOrExpr
	|	inclusiveOrExpr op = '&&' logicalAndExpr
	;

logicalOrExpr
	:	logicalAndExpr
	|	logicalAndExpr op = '||' logicalOrExpr
	;

assignmentExpr
	:	logicalOrExpr
	|	unaryExpr op = '=' assignmentExpr
	;

main
	:	translationUnit? EOF
	;

classDefinition
	:	'class' ID '{' classList '}'
	;

classList
	:	type = (TypeName | ID) Braket* name = ID ';'
	|	type = (TypeName | ID) Braket* name = ID ';' classList
	;

translationUnit
	:	functionDefinition						# funcDec
	|	variableDefinition1						# varDec
	|	classDefinition							# classDec
	|	functionDefinition translationUnit		# funcDec_
	|	variableDefinition1 translationUnit		# varDec_
	|	classDefinition translationUnit			# classDec_
	;


functionDefinition
	:	functionDefinition1 compoundStmt
	|	functionDefinition2 compoundStmt
	;

functionDefinition1
	:	funcType = (TypeName | ID) Braket* name = ID '()'
	;

functionDefinition2
	:	funcType = (TypeName | ID) Braket* name = ID '(' paramList ')'
	;

paramList
	:	type = (TypeName | ID) Braket* name = ID
	|	type = (TypeName | ID) Braket* name = ID ',' paramList
	;

variableDefinition1
	: type = (TypeName | ID) Braket* name = ID ('=' expr)? ';'
	;

variableDefinition2
	: type = (TypeName | ID) Braket* name = ID ('=' expr)? ';'
	;

Braket : '[]';
Dot : '.';

TypeName
	: Bool
	| Int
	| Void
	| String
	;

Bool : 'bool';
Int : 'int';
Void : 'void';
String : 'string';

// Constant
constant
	:	BoolConstant
	|	IntegerConstant
	|	StringConstant
	|	Null
	;

Null : 'null';

BoolConstant
	: True
	| False
	;

True : 'true';
False : 'false';

IntegerConstant
	:	'0'
	|	[1-9] Digit*
	;

StringConstant
    :   '"' StringCharacters? '"'
    ;

fragment
StringCharacters
    :   StringCharacter+
    ;
fragment
StringCharacter
    :   ~["\\]
    |   EscapeSequence
    ;

fragment
EscapeSequence
    :   '\\' [btnfr"'\\]
    ;

ID
	: Letter
	( Letter
	| Digit
	| '_'
	)*;

WS : [ \t\n\r]+ -> skip;

Comment
	: '//' ~[\r\n]*
	-> skip
	;

Letter : [a-zA-Z];
Digit : [0-9];
