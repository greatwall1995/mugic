package Checker;

import lib.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2016/4/5.
 */
public class ClassTable {
	private Symbol name;
	private Set<Symbol> set;
	public ClassTable() {
		set = new HashSet<>();
	}
	public void put(Symbol s) {
		set.add(s);
	}
	public boolean exist(Symbol e) {
		return set.contains(e);
	}
}
