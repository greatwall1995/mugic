package Checker;

import lib.*;
import AST.*;
import java.util.*;

/**
 * Created by Administrator on 2016/4/3.
 */
public class SymbolTable {
	private class Node {
		public Type type;
		private Symbol var;
		public Node() {}
		private Node(Type t, Symbol v) {
			type = t;
			var = v;
		}
		public String toString() {
			String ret = "";
			if (type != null) ret += type.toString();
			ret += " ";
			if (var != null) ret += var.toString();
			return ret;
		}

		private Symbol toSymbol() {
			return Symbol.symbol(toString());
		}
	}
	private class Edge {
		private Type value;
		private int pos;
		Edge pre;
		private Node next;
		public Edge() {}
		private Edge(Type v, int p, Edge pr, Node n) {
			value = v;
			pos = p;
			pre = pr;
			next = n;
		}
	}
	private Stack<Node> stk;
	private int height;
	private Node head;
	private static Dictionary dict = new Hashtable<>();;
	public SymbolTable() {
		stk = new Stack<>();
		height = 0;
		head = null;
	}
	public boolean put(Type type, Symbol var, Type value) {
		Node node = new Node(type, var);
		Edge t = (Edge)dict.get(node.toSymbol());
		if (t != null && t.pos == height) {
			return false;
		}
		else {
			dict.put(node.toSymbol(), new Edge(value, height, t, head));
			head = node;
			return true;
		}
	}
	public boolean put(ClassDec c) {
		for (int i = 0; i < c.list.size(); ++i) {
			if (!put(new Type(c.name, 0), c.list.get(i).name, c.list.get(i).type)) return false;
		}
		return true;
	}
	public boolean put(Symbol var, Type value) {
		return put(null, var, value);
	}
	public Type get(Type type, Symbol var) {
		Node node = new Node(type, var);
		Edge t = (Edge)dict.get(node.toSymbol());
		if (t == null) {
			return null;
		}
		return t.value;
	}
	public Type get(Symbol var) {
		return get(null, var);
	}
	public void beginScope() {
		stk.push(head);
		head = null;
		++height;
	}
	public void endScope() {
		while (head != null) {
			Edge e = (Edge)dict.get(head.toSymbol());
			if (e.pre != null) {
				dict.put(head.toSymbol(), e.pre);
			}
			else {
				dict.remove(head.toSymbol());
			}
			head = e.next;

		}
		head = stk.pop();
		--height;
	}
}
