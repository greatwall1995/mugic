package Checker;
/**
 * Created by Administrator on 2016/3/31.
 */
import java.util.*;
import Parser.*;
import AST.*;
import lib.*;

public class Checker extends MugicBaseVisitor<Integer> {
	private SymbolTable symbolTable;
	private FuncTable funcTable;
	private ClassTable classTable;
	private Set<Symbol> typeTable;
	private Prog p;
	private int compoundHeight, loopHeight;
	private Type require;
	private boolean createTable() {
		symbolTable = new SymbolTable();
		funcTable = new FuncTable();
		classTable = new ClassTable();
		typeTable = new HashSet<>();
		typeTable.add(Symbol.symbol("int"));
		typeTable.add(Symbol.symbol("bool"));
		typeTable.add(Symbol.symbol("string"));
		typeTable.add(Symbol.symbol("void"));
		for (int i = 0; i < p.decList.size(); ++i) {
			Dec t = p.decList.get(i);
			if (t instanceof FuncDec) {
				FuncDec p = (FuncDec) t;
				if (!symbolTable.put(p.name, p.type)) return false;
				funcTable.put(p);
			}
			else if (t instanceof ClassDec) {
				ClassDec p = (ClassDec) t;
				if (!symbolTable.put(p)) return false;
				typeTable.add((p.name));
				classTable.put(p.name);
			}
		}
		symbolTable.put(Symbol.symbol("getString"), new Type("string"));
		funcTable.put(Symbol.symbol("getString"), new ArrayList<Type>());
		symbolTable.put(Symbol.symbol("getInt"), new Type("int"));
		funcTable.put(Symbol.symbol("getInt"), new ArrayList<Type>());
		symbolTable.put(Symbol.symbol("print"), new Type("void"));
		funcTable.put(Symbol.symbol("print"), new ArrayList<Type>(Arrays.asList(new Type("string"))));
		symbolTable.put(Symbol.symbol("println"), new Type("void"));
		funcTable.put(Symbol.symbol("println"), new ArrayList<Type>(Arrays.asList(new Type("string"))));
		symbolTable.put(Symbol.symbol("toString"), new Type("string"));
		funcTable.put(Symbol.symbol("toString"), new ArrayList<Type>(Arrays.asList(new Type("int"))));
		return true;
	}
	public Checker() {}
	public boolean check(Prog e) {
		p = e;
		compoundHeight = loopHeight = 0;
		if (!createTable()) return false;
		for (int i = 0; i < e.decList.size(); ++i) {
			Dec t = e.decList.get(i);
			if (!visit(t)) return false;
		}
		return true;
	}
	private boolean visit(Dec e) {
		if (e instanceof FuncDec) return visit((FuncDec) e);
		else if (e instanceof ClassDec) return visit((ClassDec) e);
		else if (e instanceof VarDec1) return visit((VarDec1) e);
		return false;
	}
	private boolean visit(FuncDec e) {
		if (e.name.equals("main")) {
			if (e.param.size() > 0 || !e.type.name.equals("int")) return false;
		}
		symbolTable.beginScope();
		for (int i = 0; i < e.param.size(); ++i) {
			if (!typeTable.contains(e.param.get(i).type.name)) {
				//System.out.println("Parameter Type Error");
				return false;
			}
			if (funcTable.exist(e.param.get(i).name)) {
				//System.out.println("Parameter Name Error");
				return false;
			}
			if (!symbolTable.put(e.param.get(i).name, e.param.get(i).type)) return false;
		}
		if (!typeTable.contains(e.type.name)) {
			//System.out.println("Function Type Error");
			return false;
		}
		require = e.type;
		boolean ret = visit(e.stmt);
		symbolTable.endScope();
		return ret;
	}
	private boolean visit(ClassDec e) {
		for (int i = 0; i < e.list.size(); ++i) {
			Type t = e.list.get(i).type;
			if (!typeTable.contains(t.name)) {
				//System.out.println("Class Type Error");
				return false;
			}
		}
		return true;
	}
	private boolean visit(VarDec1 e) {
		if (!typeTable.contains(e.type.name) || e.type.name.toString().equals("void")) {
			return false;
		}
		else if (funcTable.exist(e.name)) {
			return false;
		}
		else if (e.expr == null) {
			if (!symbolTable.put(e.name, e.type)) return false;
			return true;
		}
		else {
			Type t = visit(e.expr);
			if (t == null) return false;
			if (!symbolTable.put(e.name, e.type)) return false;
			if (t.name.toString().equals("null")) {
				Symbol tt = e.type.name;
				if (tt.equals("int") || tt.equals("bool") || tt.equals("string")) {
					return e.type.dms != 0;
				}
				else return true;
			}
			else return t.equals(e.type);
		}
	}
	private Type visit(Expr e) {
		if (e instanceof BinaryExpr) return visit((BinaryExpr) e);
		else if (e instanceof LeftExpr) return visit((LeftExpr) e);
		else if (e instanceof RightExpr) return visit((RightExpr) e);
		else if (e instanceof ClassExpr) return visit((ClassExpr) e);
		else if (e instanceof ArrayExpr) return visit((ArrayExpr) e);
		else if (e instanceof CreationExpr) return visit((CreationExpr) e);
		else if (e instanceof ParenExpr) return visit((ParenExpr) e);
		else if (e instanceof NullExpr) return visit((NullExpr) e);
		else if (e instanceof StringExpr) return visit((StringExpr) e);
		else if (e instanceof IntExpr) return visit((IntExpr) e);
		else if (e instanceof BoolExpr) return visit((BoolExpr) e);
		else if (e instanceof FuncExpr) return visit((FuncExpr) e);
		else if (e instanceof VariableExpr) return visit((VariableExpr) e);
		return null;
	}
	private boolean isLvalue(Expr e) { // Now we can confirm that e is a legal expression
		if (e instanceof VariableExpr) {
			return true;
		}
		else if (e instanceof ClassExpr) {
			ClassExpr f = (ClassExpr) e;
			return isLvalue(f.left);
		}
		else if (e instanceof ArrayExpr) {
			ArrayExpr f = (ArrayExpr) e;
			return isLvalue(f.left);
		}
		else return false;
	}
	private Type visit(BinaryExpr e) {
		Type left = visit(e.left);
		Type right = visit(e.right);
		if (left == null || right == null) return null;
		else if (e.opt.equals("==") || e.opt.equals("!=")) {
			if (left.name.equals("null") || right.name.equals("null")) {
				return new Type("bool");
			}
			else if (left.name.equals(right.name) && left.dms == right.dms) {
				return new Type("bool");
			}
			else return null;
		}
		else if (e.opt.equals("=")) {
			if (right.name.equals("null")) {
				if (left.name.equals("string")) return null;
				else return left;
			}
			else if (!right.name.equals(left.name)) return null;
			if (isLvalue(e.left)) return left;
			else return null;
		}
		else if (left.dms > 0 || right.dms > 0) return null;
		else if (e.opt.equals("&&") || e.opt.equals("||")) {
			if (left.name.equals("bool") && right.name.equals("bool")) {
				return new Type("bool");
			}
			else return null;
		}
		else if (e.opt.equals("|") || e.opt.equals("^") || e.opt.equals("&")
				|| e.opt.equals("-") || e.opt.equals("*") || e.opt.equals("/")
				|| e.opt.equals("%") || e.opt.equals("<<") || e.opt.equals(">>")) {
			if (left.name.equals("int") && right.name.equals("int")) {
				return new Type("int");
			}
			else return null;
		}
		else if (e.opt.equals("+")) {
			if (left.name.equals("int") && right.name.equals("int")) {
				return new Type("int");
			}
			else if (left.name.equals("string") && right.name.equals("string")) {
				return new Type("string");
			}
			else return null;
		}
		else if (e.opt.equals("<") || e.opt.equals(">") || e.opt.equals("<=") || e.opt.equals(">=")) {
			if (left.name.equals("int") && right.name.equals("int")) {
				return new Type("bool");
			}
			else if (left.name.equals("string") && right.name.equals("string")) {
				return new Type("bool");
			}
			else return null;
		}
		return null;
	}
	private Type visit(LeftExpr e) {
		Type t = visit(e.expr);
		if (t == null || t.dms > 0) return null;
		else if (e.opt.equals("!") || e.opt.equals("!")) {
			if (t.name.equals("bool")) return t;
			else return null;
		}
		else if (e.opt.equals("+") || e.opt.equals("-") || e.opt.equals("++") || e.opt.equals("--")) {
			if (t.name.equals("int")) return t;
			else return null;
		}
		else return null;
	}
	private Type visit(RightExpr e) {
		Type t = visit(e.expr);
		if (e.opt.equals("++") || e.opt.equals("--")) {
			if (t.name.equals("int")) return t;
			else return null;
		}
		else return null;
	}
	private Type visit(ClassExpr e) {
		Type left = visit(e.left);
		//System.out.println(1);
		if (left == null) return null;
		//System.out.println(2);
		if (e.right instanceof FuncExpr) {
			FuncExpr t = (FuncExpr) e.right;
			if (left.dms > 0) {
				if (t.name.equals("size")) {
					if (t.param.size() == 0) {
						return new Type("int");
					}
					else return null;
				}
				else return null;
			}
			else if (left.name.equals("string")) {
				if (t.name.equals("length")) {
					if (t.param.size() == 0) return new Type("int");
					else return null;
				} else if (t.name.equals("substring")) {
					if (t.param.size() == 2) {
						for (int i = 0; i < 2; ++i) {
							Type p = visit(t.param.get(i));
							if (p == null || !p.name.equals("int")) return null;
						}
						return new Type("string");
					} else return null;
				} else if (t.name.equals("parseInt")) {
					if (t.param.size() == 0) {
						return new Type("int");
					} else {
						return null;
					}
				} else if (t.name.equals("ord")) {
					if (t.param.size() == 1) {
						Type p = visit(t.param.get(0));
						if (p == null || !p.name.equals("int")) return null;
						return new Type("int");
					} else return null;
				} else return null;
			}
			return null;
		}
		else if (e.right instanceof  VariableExpr){
			VariableExpr t = (VariableExpr) e.right;
			return symbolTable.get(left, t.name);
		}
		else return null;
	}
	private Type visit(ArrayExpr e) {
		Type ret = visit(e.left);
		if (ret == null || ret.dms == 0) return null;
		if (visit(e.right) == null) return null;
		else return new Type(ret.name, ret.dms - 1);
	}
	private Type visit(CreationExpr e) {
		Symbol type = e.type;
		int d = e.expr.size();
		if (d == 0) {
			if (type.equals("int") || type.equals("bool")) {
				return null;
			}
		}
		return new Type(type, d);
	}
	private Type visit(ParenExpr e) {
		return visit(e.expr);
	}
	private Type visit(NullExpr e) {
		return new Type("null");
	}
	private Type visit(StringExpr e) {
		return new Type("string");
	}
	private Type visit(IntExpr e) {
		return new Type("int");
	}
	private Type visit(BoolExpr e) {
		return new Type("bool");
	}
	private Type visit(FuncExpr e) {
		List<Type> list = new ArrayList<>();
		for (int i = 0; i < e.param.size(); ++i) {
			Type t = visit(e.param.get(i));
			if (t == null) {
				return null;
			}
			list.add(t);
		}
		if (funcTable.check(e.name, list)) {
			return symbolTable.get(e.name);
		}
		else {
			//System.out.println("Parameters error.");
			return null;
		}
	}
	private Type visit(VariableExpr e) {
		return symbolTable.get(e.name);
	}
	private boolean visit(Stmt e) {
		if (e instanceof Expr) return visit((Expr) e) != null;
		else if (e instanceof ReturnStmt) return visit((ReturnStmt) e);
		else if (e instanceof ContinueStmt) return visit((ContinueStmt) e);
		else if (e instanceof BreakStmt) return visit((BreakStmt) e);
		else if (e instanceof IfStmt) return visit((IfStmt) e);
		else if (e instanceof WhileStmt) return visit((WhileStmt) e);
		else if (e instanceof ForStmt) return visit((ForStmt) e);
		else if (e instanceof CompoundStmt) return visit((CompoundStmt) e);
		else if (e instanceof VarDec2) return visit((VarDec2) e);
		return false;
	}
	private boolean visit(ReturnStmt e) {
		if (e.expr == null) {
			return require.name.equals("void");
		}
		Type t = visit(e.expr);
		if (t == null || !t.equals(require)) return false;
		return true;
	}
	private boolean visit(ContinueStmt e) {
		return loopHeight != 0;
	}
	private boolean visit(BreakStmt e) {
		return loopHeight != 0;
	}
	private boolean visit(IfStmt e) {
		Type t = visit(e.cond);
		if (t == null || !t.equals(new Type("bool"))) return false;
		else {
			symbolTable.beginScope();
			boolean ret = visit(e.stmt1);
			symbolTable.endScope();
			if (!ret) return false;
			else if (e.stmt2 != null) {
				symbolTable.beginScope();
				ret = visit(e.stmt2);
				symbolTable.endScope();
				if (!ret) return false;
			}
		}
		return true;
	}
	private boolean visit(WhileStmt e) {
		if (!visit(e.cond).equals(new Type("bool"))) return false;
		else {
			++loopHeight;
			symbolTable.beginScope();
			boolean ret = visit(e.stmt);
			symbolTable.endScope();
			--loopHeight;
			return ret;
		}
	}
	private boolean visit(ForStmt e) {
		if (e.exp1 != null && visit(e.exp1) == null) {
			//System.out.println("exp1");
			return false;
		}
		else if (e.exp2 != null && !visit(e.exp2).equals(new Type("bool"))) {
			//System.out.println(visit(e.exp2).name.toString());
			return false;
		}
		else if (e.exp3 != null && visit(e.exp3) == null) {
			//System.out.println("exp3");
			return false;
		}
		else {
			++loopHeight;
			symbolTable.beginScope();
			boolean ret = visit(e.stmt);
			symbolTable.endScope();
			--loopHeight;
			return ret;
		}
	}
	private boolean visit(CompoundStmt e) {
		if (compoundHeight != 0) symbolTable.beginScope();
		++compoundHeight;
		for (int i = 0; i < e.stmt.size(); ++i) {
			if (!visit(e.stmt.get(i))) return false;
		}
		--compoundHeight;
		if (compoundHeight != 0) symbolTable.endScope();
		return true;
	}
	private boolean visit(VarDec2 e) {
		if (!typeTable.contains(e.type.name) || e.type.name.toString().equals("void")) {
			return false;
		}
		else if (funcTable.exist(e.name) || classTable.exist(e.name)) {
			return false;
		}
		else if (e.expr == null) {
			return symbolTable.put(e.name, e.type);
		}
		else {
			Type t = visit(e.expr);
			if (t == null) return false;
			if (!symbolTable.put(e.name, e.type)) return false;
			if (t.name.toString().equals("null")) {
				Symbol tt = e.type.name;
				if (tt.equals("int") || tt.equals("bool") || tt.equals("string")) {
					return e.type.dms != 0;
				}
				else return true;
			}
			else return t.equals(e.type);
		}
	}
}
