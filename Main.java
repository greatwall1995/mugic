/**
 * Created by Administrator on 2016/3/31.
 */
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.*;
import AST.*;
import Checker.*;
import Parser.*;
import Printer.*;
import Translater.*;

public class Main {
	public static void main(String[] args) throws Exception {
		String inputFile = null;
		if (args.length > 0) inputFile = args[0];
		InputStream is = System.in;
		if (inputFile != null) is = new FileInputStream(inputFile);
		ANTLRInputStream input = new ANTLRInputStream(is);
		MugicLexer lexer = new MugicLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MugicParser parser = new MugicParser(tokens);
		ParseTree tree = parser.main();


			ParseTreeWalker walker = new ParseTreeWalker(); // create standard walker
			BuilderListener builder = new BuilderListener(parser);
			walker.walk(builder, tree);

			//Printer printer = new Printer();
			//printer.print(builder.prog);

			//Checker checker = new Checker();
			//boolean ret = checker.check(builder.prog);
			//System.out.println(ret);
			Translater translater = new Translater();
			translater.translate(builder.prog);
			//if (ret) {
			//	System.exit(0);
			//}
			//else System.exit(1);

	}
}
