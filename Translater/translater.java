package Translater;

import AST.*;
import lib.*;
import java.util.*;

/**
 * Created by Administrator on 2016/4/25.
 */


public class Translater {

	private class Optimizer {
		List<String[]> code;
		Map<String, String> addr;

		Optimizer() {
			code = new ArrayList<>();
		}

		public void add(String[] aug) {
			code.add(aug);
			addr = new HashMap<>();
		}

		private void get_addr() {
			int tot = 1;
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				for (int j = 0; j < aug.length; ++j) {
					int t = aug[j].indexOf('$');
					if (t != -1) {
						String name = aug[j].substring(t);
						if (t != 0) {
							name = aug[j].substring(t, aug[j].length() - 1);
						}

						if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra") || name.equals("$sp")
								|| name.equals("$gp") || name.equals("$v0") || name.equals("$v1") || name.equals("$a0")
								|| name.equals("$a1") || name.equals("$a2") || name.equals("$a3")) {
							continue;
						}
						if (!addr.containsKey(name)) {
							String a = register.getAddr(name);
							if (a != null && a.contains("$gp")) addr.put(name, a);
							else addr.put(name, (-4) * (fSize / 8 + tot++) + "($sp)");
						}
					}
				}
			}
		}
		public void work() {
			erase_store_load();
			erase_load_store();
			get_addr();
			print();
		}
		private void erase_load_store() {
			int pos = -1;
			List<Integer> erase = new ArrayList<>();
			Map<String, Integer> dict = new HashMap<>();
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("lw") || aug[0].equals("lb")) {
					dict.put(aug[1], i);
					dict.put(aug[2], i);
				}
				else if (aug.length == 1 || aug[0].charAt(0) == 'b' || aug[0].charAt(0) == 'j') {
					pos = i;
				}
				else if (aug[0].equals("sw") || aug[0].equals("sb")) {
					if (!dict.containsKey(aug[1]) || !dict.containsKey(aug[2])) continue;
					int p1 = dict.get(aug[1]);
					int p2 = dict.get(aug[2]);
					if (p1 == p2 && p1 > pos) {
						erase.add(p1);
						erase.add(i);
					}
				}
				else {
					for (int j = 0; j < aug.length; ++j) {
						dict.put(aug[j], -2);
					}
				}
			}
			Collections.sort(erase, Collections.reverseOrder());
			for (int i = 0; i < erase.size(); ++i) {
				int t = erase.get(i);
				code.remove(t);
			}
		}
		private void erase_store_load() {
			int pos = -1;
			List<Integer> erase = new ArrayList<>();
			Map<String, Integer> dict = new HashMap<>();
			for (int i = 0; i < code.size(); ++i) {
				String[] aug = code.get(i);
				if (aug[0].equals("sw") || aug[0].equals("sb")) {
					dict.put(aug[1], i);
					dict.put(aug[2], i);
				}
				else if (aug.length == 1 || aug[0].charAt(0) == 'b' || aug[0].charAt(0) == 'j') {
					pos = i;
				}
				else if (aug[0].equals("lw") || aug[0].equals("lb")) {
					if (!dict.containsKey(aug[1]) || !dict.containsKey(aug[2])) continue;
					int p1 = dict.get(aug[1]);
					int p2 = dict.get(aug[2]);
					if (p1 == p2 && p1 > pos) {
						erase.add(p1);
						erase.add(i);
					}
				}
				else {
					for (int j = 0; j < aug.length; ++j) {
						dict.put(aug[j], -2);
					}
				}
			}
			Collections.sort(erase, Collections.reverseOrder());
			for (int i = 0; i < erase.size(); ++i) {
				int t = erase.get(i);
				code.remove(t);
			}
		}
		private void println(String[] aug) {
			for (int i = 0; i < aug.length; ++i) {
				if (aug[0].equals("lw") || aug[0].equals("lb")) {
					if (i == 1) continue;
				}
				int t = aug[i].indexOf("$");
				if (t != -1) {
					String name = null;
					if (t != 0) {
						name = aug[i].substring(t, aug[i].length() - 1);
					} else {
						name = aug[i].substring(t);
					}

					if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra") || name.equals("$sp")
							|| name.equals("$gp") || name.equals("$v0") || name.equals("$v1") || name.equals("$a0")
							|| name.equals("$a1") || name.equals("$a2") || name.equals("$a3")) {
						continue;
					}
					System.out.println("lw $t" + i + " " + addr.get(name));
				}
			}
			for (int i = 0; i < aug.length; ++i) {
				if (i != 0) System.out.print(" ");
				int t = aug[i].indexOf("$");
				if (t != -1) {
					String name = aug[i].substring(t);
					if (t != 0) {
						name = aug[i].substring(t, aug[i].length() - 1);
					}

					if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra") || name.equals("$sp")
							|| name.equals("$gp") || name.equals("$v0") || name.equals("$v1") || name.equals("$a0")
							|| name.equals("$a1") || name.equals("$a2") || name.equals("$a3")) {
						System.out.print(aug[i]);
						continue;
					}
					name = aug[i].substring(0, t + 1);
					if (t != 0) {
						name = name + "t" + i + ")";
					}
					else name = name + "t" + i;
					System.out.print(name);
				}
				else System.out.print(aug[i]);
			}
			System.out.println("");
			for (int i = aug.length - 1; i >= 0; --i) {
				if (aug[0].equals("sw") || aug[0].equals("sb")) {
					if (i == 1) continue;
				}
				int t = aug[i].indexOf("$");
				if (t != -1) {
					String name = aug[i].substring(t);
					if (t != 0) {
						name = aug[i].substring(t, aug[i].length() - 1);
					}

					if (name.equals("$zero") || name.equals("$fp") || name.equals("$ra") || name.equals("$sp")
							|| name.equals("$gp") || name.equals("$v0") || name.equals("$v1") || name.equals("$a0")
							|| name.equals("$a1") || name.equals("$a2") || name.equals("$a3")) {
						continue;
					}
					System.out.println("sw $t" + i + " " + addr.get(name));
				}
			}
		}
		void print() {
			for (int i = 0; i < code.size(); ++i) {
				println(code.get(i));
			}
		}
	}

	private ClassTable classTable;
	private Register register;
	private SymbolTable symbolTable;

	private int nBlock, nWord;
	private Stack<String> inLoop, outLoop;
	private static Dictionary funcTable = new Hashtable<>();
	private Stack<Register> reg;

	private int sPos, nOver;
	private final int fSize = 4000;

	private Optimizer optimizer;

	private void println(String s) {
		String[] aug = s.split(" ");
	//	System.out.println(s);
		optimizer.add(aug);
	}
	public void translate(Prog e) {
		classTable = new ClassTable();
		register = new Register();
		symbolTable = new SymbolTable();
		nBlock = 0;
		nWord = -2;
		inLoop = new Stack<>();
		outLoop = new Stack<>();
		reg = new Stack<>();
		sPos = nOver = 0;
		optimizer = new Optimizer();
		System.out.println(".text");
		System.out.println(".globl main");
		println("main:");
		for (int i = 0; i < e.decList.size(); ++i) {
			visit1(e.decList.get(i));
		}
		println("jal Main");
		println("li $v0 10");
		println("syscall");
		for (int i = 0; i < e.decList.size(); ++i) {
			visit2(e.decList.get(i));
		}
		optimizer.work();
		set_toString();
		set_println();
		set_add_string();
	}
	private void set_toString() {
		System.out.println("toString:");
		System.out.println("addi $sp $sp " + (-fSize));
		String r = "$t0";
		System.out.println("move " + r + " $a0");
		System.out.println("li $a0 20");
		System.out.println("li $v0 9");
		System.out.println("syscall");
		String r0 = "$t1";
		System.out.println("move " + r0 + " $v0");
		System.out.println("addi " + r0 + " " + r0 + " 1");
		String digit = "$t2";
		String tmp = "$t3";
		int pos = 0;
		int t3 = nBlock++;
		System.out.println("bgez " + r + " L" + t3);
		System.out.println("move $v0 " + r);
		System.out.println("L" + t3 + ":");
		for (int i = 1000000000; i > 0; i /= 10) {
			System.out.println("li " + tmp + " " + i);
			System.out.println("div " + digit + " " + r + " " + tmp);
			System.out.println("abs " + digit + " " + digit);
			System.out.println("addi " + digit + " " + digit + " 48");
			System.out.println("sb " + digit + " " + pos + "(" + r0 + ")");
			if (i != 1) System.out.println("rem " + r + " " + r + " " + tmp);
			++pos;
		}
		System.out.println("li " + digit + " 0");
		System.out.println("sb " + digit + " 10(" + r0 + ")");
		int t1 = nBlock++;
		int t2 = nBlock++;
		String r48 = "$t4";
		System.out.println("li " + r48 + " 48");
		for (int i = 0; i < 9; ++i) {
			System.out.println("lb " + digit + " 0(" + r0 + ")");
			System.out.println("bne " + digit + " " + r48 + " L" + t1);
			System.out.println("addi " + r0 + " " + r0 + " 1");
		}
		System.out.println("L" + t1 + ":");
		System.out.println("bgez $v0 L" + t2);
		System.out.println("addi " + r0 + " " + r0 + " -1");
		System.out.println("li " + digit + " 45");
		System.out.println("sb " + digit + " 0(" + r0 + ")");
		System.out.println("L" + t2 + ":");
		System.out.println("move $v0 " + r0);
		System.out.println("addi $sp $sp " + fSize);
		System.out.println("jr $ra");
	}
	void set_println() {
		System.out.println("println:");
		System.out.println("addi $sp $sp " + (-fSize));
		System.out.println("li $v0 4");
		System.out.println("syscall");
		System.out.println("li $a0 4");
		System.out.println("li $v0 9");
		System.out.println("syscall");
		System.out.println("li $v1 10");
		System.out.println("sb $v1 0($v0)");
		System.out.println("move $a0 $v0");
		System.out.println("li $v0 4");
		System.out.println("syscall");
		System.out.println("addi $sp $sp " + fSize);
		System.out.println("jr $ra");
	}
	void set_add_string() {
		System.out.println("add_string:");
		System.out.println("addi $sp $sp " + (-fSize));
		String r2 = "$t0";
		String r3 = "$t1";
		String cnt = "$t2";
		System.out.println("move " + r2 + " $a0");
		System.out.println("move " + r3 + " $a1");
		System.out.println("li " + cnt + " 0");
		String c = "$t3";
		String c1 = "$t4";
		System.out.println("move " + c + " " + r2);
		int tLoop1 = nBlock++;
		int tDone1 = nBlock++;
		System.out.println("L" + tLoop1 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone1);
		System.out.println("addi " + cnt + " " + cnt + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop1);
		System.out.println("L" + tDone1 + ":");
		System.out.println("move " + c + " " + r3);
		int tLoop2 = nBlock++;
		int tDone2 = nBlock++;
		System.out.println("L" + tLoop2 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone2);
		System.out.println("addi " + cnt + " " + cnt + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop2);
		System.out.println("L" + tDone2 + ":");
		String four = "$t5";
		System.out.println("li " + four + " 4");
		System.out.println("div " + cnt + " " + cnt + " " + four);
		System.out.println("addi " + cnt + " " + cnt + " 1");
		System.out.println("mul " + cnt + " " + cnt + " " + four);
		System.out.println("move $a0 " + cnt);
		System.out.println("li $v0 9");
		System.out.println("syscall");
		String tc = "$t6";
		System.out.println("move " + tc + " $v0");
		System.out.println("move " + c + " " + r2);
		int tLoop3 = nBlock++;
		int tDone3 = nBlock++;
		System.out.println("L" + tLoop3 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone3);
		System.out.println("sb " + c1 + " 0(" + tc + ")");
		System.out.println("addi " + tc + " " + tc + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop3);
		System.out.println("L" + tDone3 + ":");
		System.out.println("move " + c + " " + r3);
		int tLoop4 = nBlock++;
		int tDone4 = nBlock++;
		System.out.println("L" + tLoop4 + ":");
		System.out.println("lb " + c1 + " 0(" + c + ")");
		System.out.println("beqz " + c1 + " L" + tDone4);
		System.out.println("sb " + c1 + " 0(" + tc + ")");
		System.out.println("addi " + tc + " " + tc + " 1");
		System.out.println("addi " + c + " " + c + " 1");
		System.out.println("j L" + tLoop4);
		System.out.println("L" + tDone4 + ":");
		System.out.println("li " + c1 + " 0");
		System.out.println("sb " + c1 + " 0(" + tc + ")");
		System.out.println("addi $sp $sp " + fSize);
		System.out.println("jr $ra");
	}
	private void visit1(Dec e) {
		if (e instanceof VarDec1) visit((VarDec1) e);
		else if (e instanceof ClassDec) visit((ClassDec) e);
	}
	private void visit2(Dec e) {
		if (e instanceof FuncDec) visit((FuncDec) e);
	}
	private void visit(FuncDec e) {
		funcTable.put(e.name, e.type);
		if (e.name.equals("main")) println("Main:");
		else println("_" + e.name.toString() + ":");
		sPos = register.save.size();
		println("addi $fp $sp " + (-fSize));
		println("sw $sp 0($fp)");
		println("sw $ra -4($fp)");
		println("move $sp $fp");
		nWord = -2 - nOver;
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		for (int i = 0; i < e.param.size(); ++i) {
			Type type = e.param.get(i).type;
			String m = 4 * (nWord--) + "($sp)";
			String r = register.add(type, m);
			symbolTable.put(e.param.get(i).name, r);
			println("lw " + r + " " + m);
		}
		visit(e.stmt);
		symbolTable.endScope();
		register = reg.pop();
		println("lw $ra -4($sp)");
		println("lw $sp 0($sp)");
		println("jr $ra");
	}
	private void visit(ClassDec e) {
		classTable.add(e.name, e.list);
	}
	private void visit(VarDec1 e) {
		++nOver;
		if (e.type.dms == 0 && (e.type.name.equals("int") || e.type.name.equals("bool"))) {
			String m1 = 4 * (nWord--) + "($gp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				String r2 = visit(e.expr);
				println("move " + r1 + " " + r2);
			}
		} else {
			String m1 = 4 * (nWord--) + "($gp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				String r2 = visit(e.expr);
				println("move " + r1 + " " + r2);
			}
		}
	}
	private void visit(VarDec2 e) {
		if (e.type.dms == 0 && (e.type.name.equals("int") || e.type.name.equals("bool"))) {
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				String r2 = visit(e.expr);
				println("move " + r1 + " " + r2);
			}
		} else {
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = register.add(e.type, m1);
			symbolTable.put(e.name, r1);
			if (e.expr != null) {
				String r2 = visit(e.expr);
				println("move " + r1 + " " + r2);
			}
		}
	}
	private void visit(Stmt e) {
		if (e instanceof Expr) visit((Expr) e);
		else if (e instanceof ReturnStmt) visit((ReturnStmt) e);
		else if (e instanceof ContinueStmt) visit((ContinueStmt) e);
		else if (e instanceof BreakStmt) visit((BreakStmt) e);
		else if (e instanceof IfStmt) visit((IfStmt) e);
		else if (e instanceof WhileStmt) visit((WhileStmt) e);
		else if (e instanceof ForStmt) visit((ForStmt) e);
		else if (e instanceof CompoundStmt) visit((CompoundStmt) e);
		else if (e instanceof VarDec2) visit((VarDec2) e);
	}
	private void visit(ReturnStmt e) {
		if (e.expr != null) {
			String r1 = visit(e.expr);
			println("move $v0 " + r1);
		}
		println("lw $ra -4($sp)");
		println("lw $sp 0($sp)");
		println("jr $ra");
	}
	private void visit(ContinueStmt e) {
		println("j " + inLoop.peek());
	}
	private void visit(BreakStmt e) {
		println("j " + outLoop.peek());
	}
	private void visit(IfStmt e) {
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		String r = visit(e.cond);
		int t1 = nBlock++;
		int t2 = nBlock++;
		if (e.stmt2 != null) {
			println("beqz " + r + " L_Else" + t1);
		}
		else {
			println("beqz " + r + " L_IfDone" + t1);
		}
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		visit(e.stmt1);
		symbolTable.endScope();
		register = reg.pop();
		symbolTable.endScope();
		register = reg.pop();
		if (e.stmt2 != null) {
			println("j L_IfDone" + t2);
			println("L_Else" + t1 + ":");
			reg.push((Register)register.clone());
			symbolTable.beginScope();
			visit(e.stmt2);
			symbolTable.endScope();
			register = reg.pop();
			println("L_IfDone" + t2 + ":");
		}
		else println("L_IfDone" + t1 + ":");
	}
	private void visit(WhileStmt e) {
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		int t1 = nBlock++;
		println("L_While" + t1 + ":");
		String r = visit(e.cond);
		int t2 = nBlock++;
		println("beqz " + r + " L_WhileDone" + t2);
		inLoop.push("L_While" + t1);
		outLoop.push("L_WhileDone" + t2);
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		visit(e.stmt);
		symbolTable.endScope();
		register = reg.pop();
		println("j L_While" + t1);
		println("L_WhileDone" + t2 + ":");
		symbolTable.endScope();
		register = reg.pop();
		inLoop.pop();
		outLoop.pop();
	}
	private void visit(ForStmt e) {
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		if (e.exp1 != null) visit(e.exp1);
		int t1 = nBlock++;
		int t2 = nBlock++;
		println("L_For" + t1 + ":");
		String r = null;
		if (e.exp2 != null) r = visit(e.exp2);
		if (r != null) {
			println("beqz " + r + " L_For" + t2);
		}
		inLoop.push("L_For" + t1);
		outLoop.push("L_For" + t2);
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		visit(e.stmt);
		symbolTable.endScope();
		register = reg.pop();
		if (e.exp3 != null) visit(e.exp3);
		println("j L_For" + t1);
		println("L_For" + t2 + ":");
		symbolTable.endScope();
		register = reg.pop();
		inLoop.pop();
		outLoop.pop();
	}
	private void visit(CompoundStmt e) {
		reg.push((Register)register.clone());
		symbolTable.beginScope();
		for (int i = 0; i < e.stmt.size(); ++i) {
			visit(e.stmt.get(i));
		}
		symbolTable.endScope();
		register = reg.pop();
	}
	private String visit(Expr e) {
		if (e instanceof BinaryExpr) return visit((BinaryExpr) e);
		else if (e instanceof LeftExpr) return visit((LeftExpr) e);
		else if (e instanceof RightExpr) return visit((RightExpr) e);
		else if (e instanceof ClassExpr) return visit((ClassExpr) e);
		else if (e instanceof ArrayExpr) return visit((ArrayExpr) e);
		else if (e instanceof CreationExpr) return visit((CreationExpr) e);
		else if (e instanceof ParenExpr) return visit((ParenExpr) e);
		else if (e instanceof NullExpr) return visit((NullExpr) e);
		else if (e instanceof StringExpr) return visit((StringExpr) e);
		else if (e instanceof IntExpr) return visit((IntExpr) e);
		else if (e instanceof BoolExpr) return visit((BoolExpr) e);
		else if (e instanceof FuncExpr) return visit((FuncExpr) e);
		else if (e instanceof VariableExpr) return visit((VariableExpr) e);
		return "err";
	}
	private String visit(BinaryExpr e) {
		if (e.opt.equals("=")) {
			String r1 = visit(e.left);
			String r2 = visit(e.right);
			println("move " + r1 + " " + r2);
			if (r1.charAt(1) == 't') {
				println("sw " + r1 + " " + register.getAddr(r1));
			}
			return r1;
		}
		else {
			String r2 = visit(e.left);
			Type t = register.getType(r2);
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = null;
			if (!(t.name.equals("string") && t.dms == 0) && e.opt.equals("==")) {
				String r3 = visit(e.right);
				r1 = register.tadd("bool", m1);
				println("beq " + r2 + " " + r3 + " L" + nBlock);
				println("li " + r1 + " 0");
				println("j L" + (nBlock + 1));
				println("L" + (nBlock++) + ":");
				println("li " + r1 + " 1");
				println("L" + (nBlock++) + ":");
				return r1;
			}
			else if (!(t.name.equals("string") && t.dms == 0) && e.opt.equals("!=")) {
				String r3 = visit(e.right);
				r1 = register.tadd("bool", m1);
				println("bne " + r2 + " " + r3 + " L" + nBlock);
				println("li " + r1 + " 0");
				println("j L" + (nBlock + 1));
				println("L" + (nBlock++) + ":");
				println("li " + r1 + " 1");
				println("L" + (nBlock++) + ":");
				return r1;
			}
			else if (t.name.equals("int")) {
				String r3 = visit(e.right);
				if (e.opt.equals("+")) {
					r1 = register.tadd(t, m1);
					println("add " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("-")) {
					r1 = register.tadd(t, m1);
					println("sub " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("*")) {
					r1 = register.tadd(t, m1);
					println("mul " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("/")) {
					r1 = register.tadd(t, m1);
					println("div " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("%")) {
					r1 = register.tadd(t, m1);
					println("rem " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("<<")) {
					r1 = register.tadd(t, m1);
					println("sll " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals(">>")) {
					r1 = register.tadd(t, m1);
					println("srl " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("&")) {
					r1 = register.tadd(t, m1);
					println("and " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("|")) {
					r1 = register.tadd(t, m1);
					println("or " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("^")) {
					r1 = register.tadd(t, m1);
					println("xor " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("<")) {
					r1 = register.tadd("bool", m1);
					println("slt " + r1 + " " + r2 + " " + r3);
				}
				else if (e.opt.equals("<=")) {
					r1 = register.tadd("bool", m1);
					println("slt " + r1 + " " + r3 + " " + r2);
					println("xori " + r1 + " " + r1 + " " + 1);
				}
				else if (e.opt.equals(">=")) {
					r1 = register.tadd("bool", m1);
					println("slt " + r1 + " " + r2 + " " + r3);
					println("xori " + r1 + " " + r1 + " " + 1);
				}
				else if (e.opt.equals(">")) {
					r1 = register.tadd("bool", m1);
					println("slt " + r1 + " " + r3 + " " + r2);
				}
				return r1;
			}
			else if (t.name.equals("bool")) {
				r1 = register.tadd("bool", m1);
				if (e.opt.equals("&&")) {
					int tb1 = nBlock++;
					int tb2 = nBlock++;
					int tb3 = nBlock++;
					println("beqz " + r2 + " " + "L" + tb1);
					String r3 = visit(e.right);
					println("bnez " + r3 + " " + "L" + tb2);
					println("L" + tb1 + ":");
					println("li " + r1 + " 0");
					println("j L" + tb3);
					println("L" + tb2 + ":");
					println("li " + r1 + " 1");
					println("L" + tb3 + ":");
					return r1;
				}
				else if (e.opt.equals("||")) {
					int tb1 = nBlock++;
					int tb2 = nBlock++;
					int tb3 = nBlock++;
					println("bnez " + r2 + " " + "L" + tb1);
					String r3 = visit(e.right);
					println("beqz " + r3 + " " + "L" + tb2);
					println("L" + tb1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tb3);
					println("L" + tb2 + ":");
					println("li " + r1 + " 0");
					println("L" + tb3 + ":");
					return r1;
				}
				else {
					System.err.println("Unknown operation!");
					return "err";
				}
			}
			else if (t.name.equals("string")) {
				String r3 = visit(e.right);
				if (e.opt.equals("+")) {
					println("move $a0 " + r2);
					println("move $a1 " + r3);
					for (int i = 0; i < 7; ++i) {
						if (i >= register.temp.size()) break;
						println("sw $t" + i + " " + register.getAddr("$t" + i));
					}
					println("jal add_string");
					for (int i = 0; i < 7; ++i) {
						if (i >= register.temp.size()) break;
						println("lw $t" + i + " " + register.getAddr("$t" + i));
					}
					r1 = register.tadd("string", m1);
					println("move " + r1 + " $v0");
					return r1;
				}
				else if (e.opt.equals("==")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes0 = nBlock++;
					int tRes1 = nBlock++;
					int tResDone = nBlock++;
					println("bne " + now1 + " " + now2 + " L" + tRes0);
					println("j L" + tRes1);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("j L" + tResDone);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals("!=")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes1 = nBlock++;
					int tRes0 = nBlock++;
					int tResDone = nBlock++;
					println("bne " + now1 + " " + now2 + " L" + tRes1);
					println("j L" + tRes0);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tResDone);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals("<")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes1 = nBlock++;
					int tRes0 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now1 + " " + now2 + " L" + tRes1);
					println("j L" + tRes0);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tResDone);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals("<=")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes0 = nBlock++;
					int tRes1 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now2 + " " + now1 + " L" + tRes0);
					println("j L" + tRes1);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("j L" + tResDone);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals(">")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes1 = nBlock++;
					int tRes0 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now2 + " " + now1 + " L" + tRes1);
					println("j L" + tRes0);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("j L" + tResDone);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("L" + tResDone + ":");
					return r1;
				}
				else if (e.opt.equals(">=")) {
					r1 = register.tadd("bool", m1);
					String mc1 = 4 * (nWord--) + "($sp)";
					String c1 = register.tadd("char", mc1);
					String mc2 = 4 * (nWord--) + "($sp)";
					String c2 = register.tadd("char", mc2);
					String mnow1 = 4 * (nWord--) + "($sp)";
					String now1 = register.tadd("char", mnow1);
					String mnow2 = 4 * (nWord--) + "($sp)";
					String now2 = register.tadd("char", mnow2);
					println("move " + c1 + " " + r2);
					println("lb " + now1 + " 0(" + c1 + ")");
					println("move " + c2 + " " + r3);
					println("lb " + now2 + " 0(" + c2 + ")");
					int tLoop = nBlock++;
					int tDone = nBlock++;
					println("L" + tLoop + ":");
					String mtest = 4 * (nWord--) + "($sp)";
					String test = register.tadd("char", mtest);
					int tIf1 = nBlock++;
					int tIf2 = nBlock++;
					int tIf3 = nBlock++;
					println("beqz " + now1 + " L" + tIf1);
					println("bnez " + now2 + " L" + tIf2);
					println("L" + tIf1 + ":");
					println("li " + test + " 0");
					println("j L" + tIf3);
					println("L" + tIf2 + ":");
					println("li " + test + " 1");
					println("L" + tIf3 + ":");
					println("beqz " + test + " L" + tDone);
					println("bne " + now1 + " " + now2 + " L" + tDone);
					println("addi " + c1 + " " + c1 + " 1");
					println("addi " + c2 + " " + c2 + " 1");
					println("lb " + now1 + " 0(" + c1 + ")");
					println("lb " + now2 + " 0(" + c2 + ")");
					println("j L" + tLoop);
					println("L" + tDone + ":");
					int tRes0 = nBlock++;
					int tRes1 = nBlock++;
					int tResDone = nBlock++;
					println("blt " + now1 + " " + now2 + " L" + tRes0);
					println("j L" + tRes1);
					println("L" + tRes0 + ":");
					println("li " + r1 + " 0");
					println("j L" + tResDone);
					println("L" + tRes1 + ":");
					println("li " + r1 + " 1");
					println("L" + tResDone + ":");
					return r1;
				}
				return "err";
			}
			else {
				System.err.println("Unknown operation!");
				return "err";
			}
		}
	}
	private String visit(LeftExpr e) {
		String r = visit(e.expr);
		if (e.opt.equals("~")) {
			println("not " + r + " " + r);
			return r;
		}
		else if (e.opt.equals("++")) {
			println("addi " + r + " " + r + " " + 1);
			if (!(e.expr instanceof VariableExpr)) {
				println("sw " + r + " " + register.getAddr(r));
			}
			return r;
		}
		else if (e.opt.equals("--")) {
			println("addi " + r + " " + r + " " + -1);
			if (!(e.expr instanceof VariableExpr)) {
				println("sw " + r + " " + register.getAddr(r));
			}
			return r;
		}
		else if (e.opt.equals("!")) {
			println("xori " + r + " " + r + " 1");
			return r;
		}
		else if (e.opt.equals("+")) {
			return r;
		}
		else if (e.opt.equals("-")) {
			println("neg " + r + " " + r);
			return r;
		}
		else {
			System.err.println("Unknown operation!");
			return "err";
		}
	}
	private String visit(RightExpr e) {
		String r1 = visit(e.expr);
		Type t = register.getType(r1);
		String m2 = 4 * (nWord--) + "($sp)";
		String r2 = register.tadd(t, m2);
		if (e.opt.equals("++")) {
			println("move " + r2 + " " + r1);
			println("addi " + r1 + " " + r1 + " 1");
			return r2;
		}
		else if (e.opt.equals("--")) {
			println("move " + r2 + " " + r1);
			println("addi " + r1 + " " + r1 + " -1");
			return r2;
		}
		else {
			System.err.println("Unknown operation!");
			return "err";
		}
	}
	private String visit(ClassExpr e) {
		if (e.right instanceof VariableExpr) {
			String r1 = visit(e.left);
			Type t1 = register.getType(r1);
			Type t = classTable.getType(t1.name, ((VariableExpr)e.right).name);
			int offset = classTable.offset(t1.name, ((VariableExpr)e.right).name);
			String m2 = offset + "(" + r1 + ")";
			String r2 = register.tadd(t, m2);
			println("lw " + r2 + " " + m2);
			return r2;
		}
		else {
			FuncExpr e0 = (FuncExpr)e.right;
			if (e0.name.equals("size")) {
				String r1 = visit(e.left);
				String m2 = 4 * (nWord--) + "($sp)";
				String r2 = register.tadd(register.getType(r1), m2);
				println("addi " + r2 + " " + r1 + " -4");
				String m = 4 * (nWord--) + "($sp)";
				String r = register.tadd("int", m);
				println("lw " + r + " 0(" + r2 + ")");
				return r;
			}
			else if (e0.name.equals("length")) {
				String r1 = visit(e.left);
				String m = 4 * (nWord--) + "($sp)";
				String r = register.tadd("int", m);
				String m2 = 4 * (nWord--) + "($sp)";
				String r2 = register.tadd("string", m2);
				String m3 = 4 * (nWord--) + "($sp)";
				String r3 = register.tadd("char", m3);
				println("li " + r + " 0");
				println("move " + r2 + " " + r1);
				int tLoop = nBlock++;
				int tDone = nBlock++;
				println("L" + tLoop + ":");
				println("lb " + r3 + " 0(" + r2 + ")");
				println("beqz " + r3 + " L" + tDone);
				println("addi " + r + " " + r + " 1");
				println("addi " + r2 + " " + r2 + " 1");
				println("j L" + tLoop);
				println("L" + tDone + ":");
				return r;
			}
			else if (e0.name.equals("parseInt")) {
				String r1 = visit(e.left);
				String mte = 4 * (nWord--) + "($sp)";
				String te = register.tadd("string", mte);
				String mr = 4 * (nWord--) + "($sp)";
				String r = register.tadd("string", mr);
				String mc = 4 * (nWord--) + "($sp)";
				String c = register.tadd("char", mc);
				String mten = 4 * (nWord--) + "($sp)";
				String ten = register.tadd("int", mten);
				int t1 = nBlock++;
				int t2 = nBlock++;
				println("li " + ten + " 10");
				println("move " + te + " " + r1);
				println("li " + r + " 0");
				println("L" + t1 + ":");
				println("lb " + c + " 0(" + te + ")");
				println("addi " + c + " " + c + " -47");
				println("blez " + c + " L" + t2);
				println("addi " + c + " " + c + " 47");
				println("addi " + c + " " + c + " -57");
				println("bgtz " + c + " L" + t2);
				println("addi " + c + " " + c + " 57");
				println("addi " + c + " " + c + " -48");
				println("mul " + r + " " + r + " " + ten);
				println("add " + r + " " + r + " " + c);
				println("addi " + te + " " + te + " 1");
				println("j L" + t1);
				println("L" + t2 + ":");
				return r;
			}
			else if (e0.name.equals("substring")) {
				String r1 = visit(e.left);
				String rp1 = visit(e0.param.get(0));
				String rp2 = visit(e0.param.get(1));
				String mte = 4 * (nWord--) + "($sp)";
				String te = register.tadd("string", mte);
				String mr = 4 * (nWord--) + "($sp)";
				String r = register.tadd("string", mr);
				String mc = 4 * (nWord--) + "($sp)";
				String c = register.tadd("string", mc);
				String mtrp1 = 4 * (nWord--) + "($sp)";
				String trp1 = register.tadd("int", mtrp1);
				String mtrp2 = 4 * (nWord--) + "($sp)";
				String trp2 = register.tadd("int", mtrp2);
				println("move " + trp1 + " " + rp1);
				println("move " + trp2 + " " + rp2);
				println("sub $a0 " + rp2 + " " + rp1);
				println("addi $a0 $a0" + " 6");
				println("li $v0 9");
				println("syscall");
				println("move " + r + " $v0");
				println("add $v0 $v0 $a0");
				println("sb $zero -1($v0)");
				println("move $v0 " + r);
				int t1 = nBlock++;
				int t2 = nBlock++;
				int t3 = nBlock++;
				println("move " + te + " " + r1);
				println("L" + t1 + ":");
				println("bgtz " + trp1 + " L" + t2);
				println("blez " + trp2 + " L" + t3);
				println("lb " + c + " 0(" + te + ")");
				println("sb " + c + " 0($v0)");
				println("addi $v0 $v0 1");
				println("L" + t2 + ":");
				println("addi " + te + " " + te + " 1");
				println("addi " + trp1 + " " + trp1 + " -1");
				println("addi " + trp2 + " " + trp2 + " -1");
				println("j L" + t1);
				println("L" + t3 + ":");
				println("lb " + c + " 0(" + te + ")");
				println("sb " + c + " 0($v0)");
				return r;
			}
			else if (e0.name.equals("ord")) {
				String r1 = visit(e.left);
				String rp = visit(e0.param.get(0));
				String mr = 4 * (nWord--) + "($sp)";
				String r = register.tadd("int", mr);
				String mrt = 4 * (nWord--) + "($sp)";
				String rt = register.tadd("int", mrt);
				println("add " + rt + " " + rp + " " + r1);
				println("lb " + r + " 0(" + rt + ")");
				return r;
			}
			return "err";
		}
	}
	private String visit(ArrayExpr e) {
		String r2 = visit(e.left);
		String r3 = visit(e.right);
		Type t = register.getType(r2);
		--t.dms;
		String m1 = 4 * (nWord--) + "($sp)";
		String r1 = register.tadd(t, m1);
		println("add " + r1 + " " + r3 + " " + r3);
		println("add " + r1 + " " + r1 + " " + r1);
		println("add " + r1 + " " + r2 + " " + r1);
		String m4 = "0(" + r1 + ")";
		String r4 = register.tadd(t, m4);
		println("lw " + r4 + " " + m4);
		return r4;
	}
	private String visit(CreationExpr e) {
		if (e.expr.size() == 0) {
			Symbol type = e.type;
			String m = 4 * (nWord--) + "($sp)";
			String r = register.tadd(type.toString(), m);
			List<ClassDec.Var> list = classTable.getList(type);
			println("li $a0 " + 4 * list.size());
			println("li $v0 9");
			println("syscall");
			println("move " + r + " $v0");
			return r;
		}
		else {
			String m1 = 4 * (nWord--) + "($sp)";
			String r1 = register.tadd(new Type(e.type, e.expr.size()), m1);
			String r2 = visit(e.expr.get(0));
			println("move $a0 " + r2);
			println("addi $a0 $a0 1");
			println("add $a0 $a0 $a0");
			println("add $a0 $a0 $a0");
			println("li $v0 9");
			println("syscall");
			println("move " + r1 + " $v0");
			println("sw " + r2 + " 0(" + r1 + ")");
			println("addi " + r1 + " " + r1 + " 4");
			return r1;
		}
	}
	private String visit(ParenExpr e) {
		return visit(e.expr);
	}
	private String  visit(NullExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		String r = register.tadd("null", m);
		return r;
	}
	private String visit(StringExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		String r = register.tadd("string", m);
		String value = e.value.toString();
		int len = value.length();
		len = len / 4 * 4 + 4;
		println("li $a0 " + len);
		println("li $v0 9");
		println("syscall");
		println("move " + r + " $v0");
		String m_ = 4 * (nWord--) + "($sp)";
		String r_ = register.tadd("char", m_);
		for (int i = 0; i < value.length(); ++i) {
			if (value.charAt(i) == '\\') {
				if (value.charAt(i + 1) == 'n') {
					println("li " + r_ + " " + (int)'\n');
				}
				else if (value.charAt(i + 1) == '\"') {
					println("li " + r_ + " " + (int)'\"');
				}
				else if (value.charAt(i + 1) == '\\') {
					println("li " + r_ + " " + (int)'\\');
				}
				println("sb " + r_ + " " + i + "(" + r + ")");
				++i;
			}
			else {
				println("li " + r_ + " " + (int)value.charAt(i));
				println("sb " + r_ + " " + i + "(" + r + ")");
			}
		}
		println("li " + r_ + " 0");
		println("sb " + r_ + " " + value.length() + "(" + r + ")");
		return r;
	}
	private String visit(IntExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		String t = register.tadd("int", m);
		println("li " + t + " " + e.value);
		return t;
	}
	private String visit(BoolExpr e) {
		String m = 4 * (nWord--) + "($sp)";
		String t = register.tadd("bool", m);
		if (e.value) println("li " + t + " 1");
		else println("li " + t + " 0");
		return t;
	}
	private String visit(FuncExpr e) {
		if (e.name.equals("getString")) {
			println("li $a0 20");
			println("li $v0 9");
			println("syscall");
			String m = 4 * nWord-- + "($sp)";
			String r = register.tadd("string", m);
			println("move $a0 $v0");
			println("li $a1 20");
			println("li $v0 8");
			println("syscall");
			println("move " + r + " $a0");
			return r;
		}
		else if (e.name.equals("getInt")) {
			String m = 4 * nWord-- + "($sp)";
			String r = register.tadd("int", m);
			println("li $v0 5");
			println("syscall");
			println("move " + r + " $v0");
			return r;
		}
		else if (e.name.equals("print")) {
			String r = visit(e.param.get(0));
			println("move $a0 " + r);
			println("li $v0 4");
			println("syscall");
			return null;
		}
		else if (e.name.equals("println")) {
			String r = visit(e.param.get(0));
			println("move $a0 " + r);
			println("jal println");
			return null;
		}
		else if (e.name.equals("toString")) {
			String r2 = visit(e.param.get(0));
			for (int i = 0; i < 5; ++i) {
				if (i >= register.temp.size()) break;
				println("sw $t" + i + " " + register.getAddr("$t" + i));
			}
			println("move $a0 " + r2);
			println("jal toString");

			for (int i = 0; i < 5; ++i) {
				if (i >= register.temp.size()) break;
				println("lw $t" + i + " " + register.getAddr("$t" + i));
			}
			String mr = 4 * (nWord--) + "($sp)";
			String r = register.tadd("string", mr);
			println("move " + r + " $v0");
			return r;
		}
		else {
			String[] r = new String[e.param.size()];
			for (int i = 0; i < e.param.size(); ++i) {
				r[i] = visit(e.param.get(i));
			}
			for (int i = sPos; i < register.save.size(); ++i) {
				String name = "$s" + i;
				println("sw " + name + " " + register.getAddr(name));
			}
			println("addi $fp $sp " + (-fSize));
			for (int i = 0; i < e.param.size(); ++i) {
				println("sw " + r[i] + " " + (-4) * (i + 2 + nOver) +"($fp)");
			}
			println("jal _" + e.name);
			for (int i = sPos; i < register.save.size(); ++i) {
				String name = "$s" + i;
				println("lw " + name + " " + register.getAddr(name));
			}
			if (!(funcTable.get(e.name)).equals(new Type("void"))) {
				String mret = 4 * (nWord--) + "($sp)";
				String ret = register.tadd(((Type) funcTable.get(e.name)), mret);
				println("move " + ret + " $v0");
				return ret;
			}
			else return "err";
		}
	}
	private String visit(VariableExpr e) {
		return symbolTable.get(e.name);
	}
}
