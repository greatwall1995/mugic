package Translater;

import AST.ArrayExpr;
import com.sun.org.apache.regexp.internal.RE;
import lib.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/25.
 */
public class Register implements Cloneable {
	public ArrayList<Type> save, temp;
	ArrayList<String> mem_save, mem_temp;
	int nSave, nTemp;
	Register() {
		nSave = nTemp = 0;
		save = new ArrayList<>();
		temp = new ArrayList<>();
		mem_save = new ArrayList<>();
		mem_temp = new ArrayList<>();
	}
	public String add(Type type, String mem) {
		save.add(type);
		mem_save.add(mem);
		return "$s" + nSave++;
	}
	public String add(String type, String mem) {
		return add(new Type(type, 0), mem);
	}
	public String tadd(Type type, String mem) {
		temp.add(type);
		mem_temp.add(mem);
		return "$t" + nTemp++;
	}
	public String tadd(String type, String mem) {
		return tadd(new Type(type, 0), mem);
	}
	public Type getType(String s) {
		if (s.charAt(1) == 's') {
			return save.get(Integer.parseInt(s.substring(2)));
		}
		else {
			return temp.get(Integer.parseInt(s.substring(2)));
		}
	}
	public String getAddr(String s) {
		if (s.charAt(1) == 's') {
			int t = Integer.parseInt(s.substring(2));
			if (t >= mem_save.size()) return null;
			else return mem_save.get(t);
		}
		else {
			int t = Integer.parseInt(s.substring(2));
			if (t >= mem_temp.size()) return null;
			else return mem_temp.get(t);
		}
	}
	public void changeType(String s, Type type) {
		if (s.charAt(1) == 's') {
			save.set(Integer.parseInt(s.substring(2)), type);
		}
		else {
			temp.set(Integer.parseInt(s.substring(2)), type);
		}
	}
	public void changeType(String s, String type) {
		changeType(s, new Type(type, 0));
	}
	public void terase() {
		temp.remove(nTemp - 1);
		mem_temp.remove(nTemp - 1);
		--nTemp;
	}
	@Override
	protected Object clone() {
		try {
			Register reg = (Register)super.clone();
			reg.save = (ArrayList<Type>)save.clone();
			reg.temp = (ArrayList<Type>)temp.clone();
			reg.mem_save = (ArrayList<String>)mem_save.clone();
			reg.mem_temp = (ArrayList<String>)mem_temp.clone();
			return reg;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}
