package Translater;

import lib.*;
import AST.*;
import java.util.*;

/**
 * Created by Administrator on 2016/4/3.
 */
public class SymbolTable {
	private class Edge {
		private String value;
		private int pos;
		Edge pre;
		private Symbol next;
		public Edge() {}
		private Edge(String v, int p, Edge pr, Symbol n) {
			value = v;
			pos = p;
			pre = pr;
			next = n;
		}
	}
	private Stack<Symbol> stk;
	private int height;
	private Symbol head;
	private static Dictionary dict = new Hashtable<>();
	public SymbolTable() {
		stk = new Stack<>();
		height = 0;
		head = null;
	}
	public boolean put(Symbol var, String value) {
		Edge t = (Edge)dict.get(var);
		if (t != null && t.pos == height) {
			return false;
		}
		else {
			dict.put(var, new Edge(value, height, t, head));
			head = var;
			return true;
		}
	}
	public String get(Symbol var) {
		Edge t = (Edge)dict.get(var);
		if (t == null) {
			return null;
		}
		return t.value;
	}
	public void beginScope() {
		stk.push(head);
		head = null;
		++height;
	}
	public void endScope() {
		while (head != null) {
			Edge e = (Edge)dict.get(head);
			if (e.pre != null) {
				dict.put(head, e.pre);
			}
			else {
				dict.remove(head);
			}
			head = e.next;

		}
		head = stk.pop();
		--height;
	}
}
