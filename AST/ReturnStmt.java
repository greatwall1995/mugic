package AST;

/**
 * Created by Administrator on 2016/4/1.
 */
public class ReturnStmt extends Stmt {
	public Expr expr;
	ReturnStmt() {
		expr = null;
	}
	ReturnStmt(Expr e) {
		expr = e;
	}
}
