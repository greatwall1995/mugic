package AST;
import Parser.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import java.util.*;
import lib.*;
/**
 * Created by Administrator on 2016/4/1.
 */
public class BuilderListener extends MugicBaseListener {
	MugicParser parser;
	public BuilderListener(MugicParser parser) {this.parser = parser;}
	public Prog prog;
	FuncDec func;
	ClassDec cls;
	Stack<Stmt> stmtStk;
	Stack<Expr> exprStk;
	@Override
	public void enterCompound(MugicParser.CompoundContext ctx) { }
	@Override
	public void exitCompound(MugicParser.CompoundContext ctx) { }
	@Override
	public void enterFor(MugicParser.ForContext ctx) { }
	@Override
	public void exitFor(MugicParser.ForContext ctx) { }
	@Override
	public void enterWhile(MugicParser.WhileContext ctx) { }
	@Override
	public void exitWhile(MugicParser.WhileContext ctx) { }
	@Override
	public void enterIf(MugicParser.IfContext ctx) { }
	@Override
	public void exitIf(MugicParser.IfContext ctx) { }
	@Override
	public void enterJump(MugicParser.JumpContext ctx) { }
	@Override
	public void exitJump(MugicParser.JumpContext ctx) { }
	@Override
	public void enterVarDef(MugicParser.VarDefContext ctx) { }
	@Override
	public void exitVarDef(MugicParser.VarDefContext ctx) { }
	@Override
	public void enterEp(MugicParser.EpContext ctx) { }
	@Override
	public void exitEp(MugicParser.EpContext ctx) {
		stmtStk.add(exprStk.pop());
	}
	@Override
	public void enterCompoundStmt(MugicParser.CompoundStmtContext ctx) { }
	@Override
	public void exitCompoundStmt(MugicParser.CompoundStmtContext ctx) {
		CompoundStmt compoundStmt = new CompoundStmt();
		List<Stmt> tmp = new ArrayList<>();
		for (int i = ctx.stmt().size() - 1; i >= 0; --i) {
			tmp.add(stmtStk.pop());
		}
		for (int i = tmp.size() - 1; i >= 0; --i) {
			compoundStmt.stmt.add(tmp.get(i));
		}
		stmtStk.push(compoundStmt);
	}
	@Override
	public void enterForStmt(MugicParser.ForStmtContext ctx) { }
	@Override
	public void exitForStmt(MugicParser.ForStmtContext ctx) {
		ForStmt forStmt = new ForStmt();
		if (ctx.exp3 != null) {
			forStmt.exp3 = exprStk.pop();
		}
		if (ctx.exp2 != null) {
			forStmt.exp2 = exprStk.pop();
		}
		if (ctx.exp1 != null) {
			forStmt.exp1 = exprStk.pop();
		}
		forStmt.stmt = stmtStk.pop();
		stmtStk.push(forStmt);
	}
	@Override
	public void enterWhileStmt(MugicParser.WhileStmtContext ctx) { }
	@Override
	public void exitWhileStmt(MugicParser.WhileStmtContext ctx) {
		WhileStmt whileStmt = new WhileStmt();
		whileStmt.cond = exprStk.pop();
		whileStmt.stmt = stmtStk.pop();
		stmtStk.push(whileStmt);
	}
	@Override
	public void enterIfStmt(MugicParser.IfStmtContext ctx) { }
	@Override
	public void exitIfStmt(MugicParser.IfStmtContext ctx) {
		IfStmt ifStmt = new IfStmt();
		ifStmt.cond = exprStk.pop();
		if (ctx.stmt2 != null) {
			ifStmt.stmt2 = stmtStk.pop();
		}
		else {
			ifStmt.stmt2 = null;
		}
		ifStmt.stmt1 = stmtStk.pop();
		stmtStk.add(ifStmt);
	}
	@Override
	public void enterBreak(MugicParser.BreakContext ctx) { }
	@Override
	public void exitBreak(MugicParser.BreakContext ctx) {
		stmtStk.add(new BreakStmt());
	}
	@Override
	public void enterContinue(MugicParser.ContinueContext ctx) { }
	@Override
	public void exitContinue(MugicParser.ContinueContext ctx) {
		stmtStk.add(new ContinueStmt());
	}
	@Override
	public void enterReturn(MugicParser.ReturnContext ctx) { }
	@Override
	public void exitReturn(MugicParser.ReturnContext ctx) {
		ReturnStmt returnStmt;
		if (ctx.expr() != null) returnStmt = new ReturnStmt(exprStk.pop());
		else returnStmt = new ReturnStmt();
		stmtStk.add(returnStmt);
	}
	@Override
	public void enterExpr(MugicParser.ExprContext ctx) { }
	@Override
	public void exitExpr(MugicParser.ExprContext ctx) { }
	@Override
	public void enterVariableExpr(MugicParser.VariableExprContext ctx) { }
	@Override
	public void exitVariableExpr(MugicParser.VariableExprContext ctx) {
		VariableExpr expr = new VariableExpr();
		expr.name = Symbol.symbol(ctx.ID().getText());
		exprStk.push(expr);
	}
	@Override
	public void enterFuncExpr1(MugicParser.FuncExpr1Context ctx) { }
	@Override
	public void exitFuncExpr1(MugicParser.FuncExpr1Context ctx) {
		FuncExpr expr = new FuncExpr();
		expr.name = Symbol.symbol(ctx.ID().getText());
		exprStk.push(expr);
	}
	@Override
	public void enterFuncExpr2(MugicParser.FuncExpr2Context ctx) { }
	@Override
	public void exitFuncExpr2(MugicParser.FuncExpr2Context ctx) {
		FuncExpr expr = new FuncExpr();
		expr.name = Symbol.symbol(ctx.ID().getText());
		List<Expr> tmp = new ArrayList<>();
		for (int i = ctx.expr().size() - 1; i >= 0; --i) {
			tmp.add(exprStk.pop());
		}
		for (int i = ctx.expr().size() - 1; i >= 0; --i) {
			expr.param.add(tmp.get(i));
		}
		exprStk.push(expr);
	}
	@Override
	public void enterConstantExpr(MugicParser.ConstantExprContext ctx) { }
	@Override
	public void exitConstantExpr(MugicParser.ConstantExprContext ctx) {
		if (ctx.constant().BoolConstant() != null) {
			BoolExpr expr = new BoolExpr();
			expr.value = ctx.constant().BoolConstant().getText().equals("true");
			exprStk.push(expr);
		}
		else if (ctx.constant().IntegerConstant() != null) {
			IntExpr expr = new IntExpr();
			expr.value = Integer.parseInt(ctx.constant().IntegerConstant().getText());
			exprStk.push(expr);
		}
		else if (ctx.constant().StringConstant() != null) {
			StringExpr expr = new StringExpr();
			String s = ctx.constant().StringConstant().getText();
			int len = s.length();
			expr.value = Symbol.symbol(s.substring(1, len - 1));
			exprStk.push(expr);
		}
		else if (ctx.constant().Null() != null) {
			exprStk.push(new NullExpr());
		}
	}
	@Override
	public void enterParenExpr(MugicParser.ParenExprContext ctx) { }
	@Override
	public void exitParenExpr(MugicParser.ParenExprContext ctx) {
		ParenExpr expr = new ParenExpr();
		expr.expr = exprStk.pop();
		exprStk.push(expr);
	}
	@Override
	public void enterCreationExpr(MugicParser.CreationExprContext ctx) { }
	@Override
	public void exitCreationExpr(MugicParser.CreationExprContext ctx) {
		CreationExpr expr = new CreationExpr();
		expr.type = Symbol.symbol(ctx.type.getText());
		List<Expr> tmp = new ArrayList<>();
		for (int i = ctx.expr().size() - 1; i >= 0; --i) {
			tmp.add(exprStk.pop());
		}
		for (int i = ctx.expr().size() - 1; i >= 0; --i) {
			expr.expr.add(tmp.get(i));
		}
		for (int i = 0; i < ctx.Braket().size(); ++i) {
			expr.expr.add(null);
		}
		exprStk.push(expr);
	}
	@Override
	public void enterPrExpr(MugicParser.PrExprContext ctx) { }
	@Override
	public void exitPrExpr(MugicParser.PrExprContext ctx) { }
	@Override
	public void enterArrayExpr(MugicParser.ArrayExprContext ctx) { }
	@Override
	public void exitArrayExpr(MugicParser.ArrayExprContext ctx) {
		ArrayExpr expr = new ArrayExpr();
		expr.right = exprStk.pop();
		expr.left = (UnaryExpr)exprStk.pop();
		exprStk.push(expr);
	}
	@Override
	public void enterClassExpr(MugicParser.ClassExprContext ctx) { }
	@Override
	public void exitClassExpr(MugicParser.ClassExprContext ctx) {
		ClassExpr expr = new ClassExpr();
		expr.right = (SingleExpr)exprStk.pop();
		expr.left = (UnaryExpr)exprStk.pop();
		exprStk.push(expr);
	}
	@Override
	public void enterRightExpr(MugicParser.RightExprContext ctx) { }
	@Override
	public void exitRightExpr(MugicParser.RightExprContext ctx) {
		RightExpr expr = new RightExpr();
		expr.opt = Symbol.symbol(ctx.op.getText());
		expr.expr = (UnaryExpr)exprStk.pop();
		exprStk.push(expr);
	}
	@Override
	public void enterUnaryExpr(MugicParser.UnaryExprContext ctx) { }
	@Override
	public void exitUnaryExpr(MugicParser.UnaryExprContext ctx) {
		if (ctx.op != null) {
			LeftExpr expr = new LeftExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.expr = (UnaryExpr)exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterMultiplicativeExpr(MugicParser.MultiplicativeExprContext ctx) { }
	@Override
	public void exitMultiplicativeExpr(MugicParser.MultiplicativeExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterAdditiveExpr(MugicParser.AdditiveExprContext ctx) { }
	@Override
	public void exitAdditiveExpr(MugicParser.AdditiveExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterShiftExpr(MugicParser.ShiftExprContext ctx) { }
	@Override
	public void exitShiftExpr(MugicParser.ShiftExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterRelationalExpr(MugicParser.RelationalExprContext ctx) { }
	@Override
	public void exitRelationalExpr(MugicParser.RelationalExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterEqualityExpr(MugicParser.EqualityExprContext ctx) { }
	@Override
	public void exitEqualityExpr(MugicParser.EqualityExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterAndExpr(MugicParser.AndExprContext ctx) { }
	@Override
	public void exitAndExpr(MugicParser.AndExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterExclusiveOrExpr(MugicParser.ExclusiveOrExprContext ctx) { }
	@Override
	public void exitExclusiveOrExpr(MugicParser.ExclusiveOrExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterInclusiveOrExpr(MugicParser.InclusiveOrExprContext ctx) { }
	@Override
	public void exitInclusiveOrExpr(MugicParser.InclusiveOrExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterLogicalAndExpr(MugicParser.LogicalAndExprContext ctx) { }
	@Override
	public void exitLogicalAndExpr(MugicParser.LogicalAndExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterLogicalOrExpr(MugicParser.LogicalOrExprContext ctx) { }
	@Override
	public void exitLogicalOrExpr(MugicParser.LogicalOrExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterAssignmentExpr(MugicParser.AssignmentExprContext ctx) { }
	@Override
	public void exitAssignmentExpr(MugicParser.AssignmentExprContext ctx) {
		if (ctx.op != null) {
			BinaryExpr expr = new BinaryExpr();
			expr.opt = Symbol.symbol(ctx.op.getText());
			expr.right = exprStk.pop();
			expr.left = exprStk.pop();
			exprStk.push(expr);
		}
	}
	@Override
	public void enterMain(MugicParser.MainContext ctx) {
		prog = new Prog();
		stmtStk = new Stack<>();
		exprStk = new Stack<>();
	}
	@Override
	public void exitMain(MugicParser.MainContext ctx) { }
	@Override
	public void enterClassDefinition(MugicParser.ClassDefinitionContext ctx) {
		cls = new ClassDec();
		cls.name = Symbol.symbol(ctx.ID().getText());
	}
	@Override
	public void exitClassDefinition(MugicParser.ClassDefinitionContext ctx) {
		prog.decList.add(cls);
	}
	@Override
	public void enterClassList(MugicParser.ClassListContext ctx) {
		Type type = new Type(ctx.type.getText(), ctx.Braket().size());
		cls.add(type, ctx.name.getText());
	}
	@Override
	public void exitClassList(MugicParser.ClassListContext ctx) { }
	@Override
	public void enterFuncDec(MugicParser.FuncDecContext ctx) { }
	@Override
	public void exitFuncDec(MugicParser.FuncDecContext ctx) { }
	@Override
	public void enterVarDec(MugicParser.VarDecContext ctx) { }
	@Override
	public void exitVarDec(MugicParser.VarDecContext ctx) { }
	@Override
	public void enterClassDec(MugicParser.ClassDecContext ctx) { }
	@Override
	public void exitClassDec(MugicParser.ClassDecContext ctx) { }
	@Override
	public void enterFuncDec_(MugicParser.FuncDec_Context ctx) { }
	@Override
	public void exitFuncDec_(MugicParser.FuncDec_Context ctx) { }
	@Override
	public void enterVarDec_(MugicParser.VarDec_Context ctx) { }
	@Override
	public void exitVarDec_(MugicParser.VarDec_Context ctx) { }
	@Override
	public void enterClassDec_(MugicParser.ClassDec_Context ctx) { }
	@Override
	public void exitClassDec_(MugicParser.ClassDec_Context ctx) { }
	@Override
	public void enterFunctionDefinition(MugicParser.FunctionDefinitionContext ctx) {
		func = new FuncDec();
	}
	@Override
	public void exitFunctionDefinition(MugicParser.FunctionDefinitionContext ctx) {
		func.stmt = stmtStk.pop();
		prog.decList.add(func);
	}
	@Override
	public void enterFunctionDefinition1(MugicParser.FunctionDefinition1Context ctx) { }
	@Override
	public void exitFunctionDefinition1(MugicParser.FunctionDefinition1Context ctx) {
		func.type = new Type(ctx.funcType.getText(), ctx.Braket().size());
		func.name = Symbol.symbol(ctx.name.getText());
	}
	@Override
	public void enterFunctionDefinition2(MugicParser.FunctionDefinition2Context ctx) { }
	@Override
	public void exitFunctionDefinition2(MugicParser.FunctionDefinition2Context ctx) {
		func.type = new Type(ctx.funcType.getText(), ctx.Braket().size());
		func.name = Symbol.symbol(ctx.name.getText());
	}
	@Override
	public void enterParamList(MugicParser.ParamListContext ctx) {
		Type type = new Type(ctx.type.getText(), ctx.Braket().size());
		func.addParam(type, ctx.name.getText());
	}
	@Override
	public void exitParamList(MugicParser.ParamListContext ctx) { }
	@Override
	public void enterVariableDefinition1(MugicParser.VariableDefinition1Context ctx) { }
	@Override
	public void exitVariableDefinition1(MugicParser.VariableDefinition1Context ctx) {
		VarDec1 var1 = new VarDec1();
		var1.type = new Type(ctx.type.getText(), ctx.Braket().size());
		var1.name = Symbol.symbol(ctx.name.getText());
		if (ctx.expr() == null) {
			var1.expr = null;
		}
		else {
			var1.expr = exprStk.pop();
		}
		prog.decList.add(var1);
	}
	@Override
	public void enterVariableDefinition2(MugicParser.VariableDefinition2Context ctx) { }
	@Override
	public void exitVariableDefinition2(MugicParser.VariableDefinition2Context ctx) {
		VarDec2 var2 = new VarDec2();
		var2.type = new Type(ctx.type.getText(), ctx.Braket().size());
		var2.name = Symbol.symbol(ctx.name.getText());
		if (ctx.expr() == null) {
			var2.expr = null;
		}
		else {
			var2.expr = exprStk.pop();
		}
		stmtStk.add(var2);
	}
	@Override
	public void enterEveryRule(ParserRuleContext ctx) { }
	@Override
	public void exitEveryRule(ParserRuleContext ctx) { }
	@Override
	public void visitTerminal(TerminalNode node) { }
	@Override
	public void visitErrorNode(ErrorNode node) { }
}
