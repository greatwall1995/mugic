package AST;

import java.util.List;

/**
 * Created by Administrator on 2016/4/2.
 */
public class ArrayExpr extends UnaryExpr{
	public UnaryExpr left;
	public Expr right;
	public ArrayExpr() {}
	public ArrayExpr(UnaryExpr l, Expr r) {
		left = l;
		right = r;
	}
	public boolean isLvalue() {
		return true;
	}
}
