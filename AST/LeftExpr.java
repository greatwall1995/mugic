package AST;

import lib.*;

/**
 * Created by Administrator on 2016/4/2.
 */
public class LeftExpr extends UnaryExpr{
	public Symbol opt;
	public UnaryExpr expr;
	public LeftExpr() {}
	@Override
	public boolean isLvalue() {
		return true;
	}
}
