package AST;

import java.util.ArrayList;
import java.util.List;
import lib.*;

/**
 * Created by Administrator on 2016/4/2.
 */
public class FuncExpr extends SingleExpr {
	public Symbol name;
	public List<Expr> param;
	public FuncExpr() {
		param = new ArrayList<Expr>();
	}
	public FuncExpr(String n, List<Expr> p) {
		name = Symbol.symbol(n);
		param = p;
	}
	@Override
	public boolean isLvalue() {
		return false;
	}
}
