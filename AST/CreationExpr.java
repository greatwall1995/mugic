package AST;

import lib.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/4/2.
 */
public class CreationExpr extends SingleExpr {
	public Symbol type;
	public List<Expr> expr;
	public CreationExpr() {
		expr = new ArrayList<Expr>();
	}
	public CreationExpr(String t, List<Expr> e) {
		type = Symbol.symbol(t);
		expr = e;
	}
	@Override
	public boolean isLvalue() {
		return false;
	}
}
